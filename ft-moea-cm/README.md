# FT-MOEA-CM

The approach presented in this algorithm aims to infer fault tree models from given failure data sets. Based on the implementation of FT-MOEA, FT-MOEA-CM introduces the use of confusion matrix metrics instead of metrics based on Minimal Cut Sets.
