# Inference of fault tree models

The approaches presented here aim to infer fault tree models from given failure data sets.

## Repository overview

This repository contains the following folders corresponding to different approaches:
- [ft_moea](ft_moea) contains the code and results for the *FT-MOEA*, an algorithm inferring fault tree models using multi-objective evolutionary algorithms.
- [symlearn](symlearn) contains the code and results for *SymLearn*, an inference algorithm using symmetries and modules.
